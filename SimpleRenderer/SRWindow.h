/*
This structure represents the window that is rendered
*/
typedef struct{
  char ** colorLayer;
  char ** textLayer;
  int windowWidth;
  int windowHeight;
  char backgroundColor;
}SRWindow;
