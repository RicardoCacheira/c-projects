#include <stdio.h>
#include <stdlib.h>
#include "simpleRenderer.h"

int main(void) {
  SRWindow * window = inicializeWindow(16,9);
  drawHorizontalLine(window,DARK_YELLOW_CODE,3,1,10);
  drawVerticalLine(window,DARK_CYAN_CODE,5,2,3);
  drawRectangle(window,LIGHT_BLUE_CODE,0,0,15,8);
  drawFilledRectangle(window,GREEN_CODE,2,2,11,5);
  drawSquare(window,DARK_RED_CODE,12,5,3);
  drawPixel(window,DARK_GRAY_CODE,15,8);
  drawChar(window,'v',1,1,0);
  drawChar(window,'A',1,0,1);
  drawString(window,"Hello World!",1,1,1);
  drawString(window,"Hello World!",1,2,0);
  drawString(window,"Hello World!",1,3,1);
  renderWindow(window);
  clearWindow(window);
  setBackgroundColor(window,RED_CODE);
  SRWindow * window2 = inicializeWindow(30,16);
  drawFilledRectangle(window2,CYAN_CODE,1,1,28,14);
  drawString(window2,"Second Window",10,7,1);
  drawLine(window2,RED_CODE,0,0,29,15,XY_BASED_DRAW);
  drawLine(window2,BLUE_CODE,0,15,29,0,X_BASED_DRAW);
  drawLine(window2,GREEN_CODE,0,0,7,15,Y_BASED_DRAW);
  setBackgroundColor(window2,BLACK_CODE);
  getchar();
  renderWindow(window2);
  exportWindow(window2,"win2.txt");
  getchar();
  renderWindow(window);
  clearWindow(window2);
  getchar();
  renderWindow(window2);
  getchar();
  finalizeWindow(window);
  finalizeWindow(window2);
  SRWindow * window3 = importWindow("win2.txt");
  renderWindow(window3);
  getchar();
  finalizeWindow(window3);
}
