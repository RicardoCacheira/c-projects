#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include "SRWindow.h"

#define RESET_CURSOR "\e[1;1H"
#define CLEAR_TERM "\e[2J"

#define TERMINAL_DEFAULT "\e[0m"
#define BLACK "\e[1;37;40m"
#define DARK_RED "\e[1;37;41m"
#define DARK_GREEN "\e[22;30;42m"
#define DARK_YELLOW "\e[22;30;43m"
#define BLUE "\e[1;37;44m"
#define DARK_MAGENTA "\e[1;37;45m"
#define DARK_CYAN "\e[22;30;46m"
#define LIGHT_GRAY "\e[22;30;47m"
#define DARK_GRAY "\e[1;37;100m"
#define RED "\e[1;37;101m"
#define GREEN "\e[22;30;102m"
#define YELLOW "\e[22;30;103m"
#define LIGHT_BLUE "\e[1;37;104m"
#define MAGENTA "\e[1;37;105m"
#define CYAN "\e[22;30;106m"
#define WHITE "\e[22;30;107m"

#define TERMINAL_DEFAULT_CODE 0
#define BLACK_CODE 1
#define DARK_RED_CODE 2
#define DARK_GREEN_CODE 3
#define DARK_YELLOW_CODE 4
#define BLUE_CODE 5
#define DARK_MAGENTA_CODE 6
#define DARK_CYAN_CODE 7
#define LIGHT_GRAY_CDOE 8
#define DARK_GRAY_CODE 9
#define RED_CODE 10
#define GREEN_CODE 11
#define YELLOW_CODE 12
#define LIGHT_BLUE_CODE 13
#define MAGENTA_CODE 14
#define CYAN_CODE 15
#define WHITE_CODE 16

SRWindow * inicializeWindow(int winWidth,int winHeight){
  SRWindow * window=calloc(1,sizeof(SRWindow));
  window->backgroundColor=0;
  window->windowWidth=winWidth;
  window->windowHeight=winHeight;
  window->colorLayer=calloc(winHeight,sizeof(char*));
  window->textLayer=calloc(winHeight,sizeof(char*));
  int i,j;
  for (i = 0; i < winHeight; i++) {
    *(window->colorLayer+i)=calloc(winWidth,sizeof(char));
    *(window->textLayer+i)=calloc(winWidth*2,sizeof(char));
    int charNum=winWidth*2;
    for (j = 0; j < charNum; j++) {
      *(*(window->textLayer+i)+j)=' ';
    }
  }
  return window;
}

void clearWindow(SRWindow * window){
  int i,j;
  for (i = 0; i < window->windowHeight; i++) {
    for (j = 0; j < window->windowWidth; j++) {
      *(*(window->colorLayer+i)+j)=0;
    }
    int charNum=window->windowWidth*2;
    for (j = 0; j < charNum; j++) {
      *(*(window->textLayer+i)+j)=' ';
    }
  }
}

void setBackgroundColor(SRWindow * window, int backgroundColorCode){
  window->backgroundColor=backgroundColorCode;
}

void exportWindow(SRWindow * window, char * filename){
  unsigned int aux;
  FILE *fp;
  fp = fopen(filename, "w");
  fprintf(fp, "%dx%d-%d\n", window->windowWidth, window->windowHeight, window->backgroundColor);
  for (size_t i = 0; i < window->windowHeight; i++) {
    for (size_t j = 0; j < window->windowWidth; j++) {
      //bits 16-23: Pixel Color Value, bits 8-15: Pixel 1st Charcter, bits 0-7: Pixel 2nd Charcter
      aux=0;
      aux=*(*(window->colorLayer+i)+j)<<8;
      aux+=*(*(window->textLayer+i)+j*2);
      aux=aux<<8;
      aux+=*(*(window->textLayer+i)+j*2+1);
      fprintf(fp, "%06X", aux);
    }
    fprintf(fp, "\n");
  }
  fclose(fp);
}

SRWindow * importWindow(char * filename){
  unsigned int aux;
  int winWidth, winHeight;
  int backgroundColor;
  FILE *fp;
  fp = fopen(filename, "r");
  fscanf(fp, "%dx%d-%d\n", &winWidth, &winHeight, &backgroundColor);
  SRWindow * window = inicializeWindow(winWidth, winHeight);
  setBackgroundColor(window, backgroundColor);
  int winWidth2=winWidth*2;
  for (size_t i = 0; i < window->windowHeight; i++) {
    for (size_t j = 0; j < window->windowWidth; j++) {
      fscanf(fp, "%06X", &aux);
      *(*(window->textLayer+i)+j*2+1)=aux&0xFF;
      aux=aux>>8;
      *(*(window->textLayer+i)+j*2)=aux&0xFF;
      aux=aux>>8;
      *(*(window->colorLayer+i)+j)=aux;
    }
  }
  fclose(fp);
  return window;
}

void finalizeWindow(SRWindow * window){
  int i;
  for (i = 0; i < window->windowHeight; i++) {
    free(*(window->colorLayer+i));
    free(*(window->textLayer+i));
  }
  free(window->colorLayer);
  free(window->textLayer);
  free(window);
  printf(TERMINAL_DEFAULT CLEAR_TERM RESET_CURSOR);
}

void drawPixel(SRWindow * window, int pixelColorCode, int posX, int posY){
  *((*( window->colorLayer + (posY))) + posX)=pixelColorCode;
}

void drawVerticalLine(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght){
  int i;
  for ( i = 0; i < lenght; i++) {
    drawPixel(window, pixelColorCode, posX, posY+i);
  }
}

void drawHorizontalLine(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght){
  int i;
  for ( i = 0; i < lenght; i++) {
    drawPixel(window, pixelColorCode, posX+i, posY);
  }
}

void drawLine(SRWindow * window, int pixelColorCode, int posXa, int posYa, int posXb, int posYb, int drawMode){
  double b=((double)(posYb-posYa))/(posXb-posXa),m=posYa-b*posXa;
  int min,max,i;
  if(drawMode%2==0){
    if(posXb<posXa){
      min=posXb;
      max=posXa;
    }
    min=posXa;
    max=posXb;
    for ( i = min; i <= max; i++) {
      drawPixel(window, pixelColorCode, i, (int)(b*i+m));
    }
  }
  if(drawMode>0){
    if(posYb<posYa){
      min=posYb;
      max=posYa;
    }
    min=posYa;
    max=posYb;
    for ( i = min; i <= max; i++) {
      drawPixel(window, pixelColorCode, (int)((i-m)/b), i);
    }
  }
}

void drawRectangle(SRWindow * window, int pixelColorCode, int posX, int posY, int width, int height){
  drawVerticalLine(window, pixelColorCode, posX, posY, height);
  drawVerticalLine(window ,pixelColorCode, posX+width-1, posY, height);
  drawHorizontalLine(window, pixelColorCode, posX, posY, width);
  drawHorizontalLine(window, pixelColorCode, posX, posY+height-1, width);
}

void drawFilledRectangle(SRWindow * window, int pixelColorCode, int posX, int posY, int width, int height){
  int i;
  for (i = 0; i < height; i++) {
    drawHorizontalLine(window, pixelColorCode, posX, posY+i, width);
  }
}

void drawSquare(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght){
  drawRectangle(window, pixelColorCode, posX, posY, lenght, lenght);
}

void drawFilledSquare(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght){
  drawFilledRectangle(window, pixelColorCode, posX, posY, lenght, lenght);
}

void drawChar(SRWindow * window, char charToDraw, int posX, int posY,int alignToColorLayer){
  if(alignToColorLayer){
    *((*( window->textLayer + posY)) + (posX*2))=charToDraw;
  }else{
    *((*( window->textLayer + posY)) + posX)=charToDraw;
  }
}

void drawString(SRWindow * window, char* str, int posX, int posY,int alignToColorLayer){
  char * aux=str;
  char * pos;
  if(alignToColorLayer){
    pos=((*( window->textLayer + posY)) + (posX*2));
  }else{
    pos=((*( window->textLayer + posY)) + posX);
  }
  while(*aux!=0){
    *pos=*aux;
    pos++;
    aux++;
  }
}

char * codeToColor(int code){
  switch(code){
    case BLACK_CODE :
      return BLACK;
    case DARK_RED_CODE :
      return DARK_RED;
    case DARK_GREEN_CODE :
      return DARK_GREEN;
    case DARK_YELLOW_CODE :
      return DARK_YELLOW;
    case BLUE_CODE :
      return BLUE;
    case DARK_MAGENTA_CODE :
      return DARK_MAGENTA;
    case DARK_CYAN_CODE :
      return DARK_CYAN;
    case LIGHT_GRAY_CDOE :
      return LIGHT_GRAY;
    case DARK_GRAY_CODE :
      return DARK_GRAY;
    case RED_CODE :
      return RED;
    case GREEN_CODE :
      return GREEN;
    case YELLOW_CODE :
      return YELLOW;
    case LIGHT_BLUE_CODE :
      return LIGHT_BLUE;
    case MAGENTA_CODE :
      return MAGENTA;
    case CYAN_CODE :
      return CYAN;
    case WHITE_CODE :
      return WHITE;
    default:
      return TERMINAL_DEFAULT;
  }
}

void renderWindow(SRWindow * window){
  struct winsize w;
  ioctl(0, TIOCGWINSZ, &w);
  int alignTop=(w.ws_row-window->windowHeight)/2;
  int bottomExtra=(w.ws_row-window->windowHeight)%2;
  int alignLeft=(w.ws_col-window->windowWidth*2)/2;
  int rightExtra=(w.ws_col-window->windowWidth*2)%2;
  int notEnoughHeight=w.ws_row<window->windowHeight,notEnoughWidth=w.ws_col<(window->windowWidth*2);
  if(notEnoughHeight && notEnoughWidth){
    printf(TERMINAL_DEFAULT CLEAR_TERM RESET_CURSOR"Not enough height and width.");
  }else if(notEnoughHeight){
    printf(TERMINAL_DEFAULT CLEAR_TERM RESET_CURSOR"Not enough height.");
  }else if(notEnoughWidth){
    printf(TERMINAL_DEFAULT CLEAR_TERM RESET_CURSOR"Not enough width.");
  }else{
    int i, j, x=w.ws_col;
    char line[x+1];
    for ( i = 0; i < x; i++) {
      line[i]=' ';
    }
    line[x]=0;
    printf("%s",codeToColor(window->backgroundColor));
    printf(RESET_CURSOR);
    for (i = 0; i < alignTop; i++) {
      printf("%s\n", line);
    }
    int charNum=window->windowWidth*2;
    for (i = 0; i < window->windowHeight; i++) {
      printf("%s", codeToColor(window->backgroundColor));
      for (j = 0; j < alignLeft; j++) {
        printf(" ");
      }
      for (j = 0; j < charNum; j++) {
        printf("%s%c",codeToColor(*(*(window->colorLayer+i)+(j/2))), *(*(window->textLayer+i)+j)  );

      }
      printf("%s", codeToColor(window->backgroundColor));
      for ( j = 0; j < alignLeft; j++) {
        printf(" ");
      }
      if(rightExtra==1){
        printf(" ");
      }
      if(i != window->windowHeight-1){
        printf("\n");
      }
    }
    if(0 < alignTop-1){
      printf("\n");
    }
    for (i = 0; i < alignTop-1; i++) {
      printf("%s\n", line);
    }
    if(bottomExtra==1){
      printf("%s", line);
      if(alignTop!=0){
        printf("\n%s", line);
      }

    }else{
      if(alignTop!=0){
        printf("%s", line);
      }
    }
  }
}
