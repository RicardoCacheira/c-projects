#include <stdlib.h>

int * game;
void startGame(){
  game=calloc(10,sizeof(int));
  game[0]=1;
}

int play(opc){
  if(game[opc]==0){
    game[opc]=game[0];
    game[0]=game[0]^3;//Change player (1^3=2, 2^3=1)
    int i,win=0;
    for (i = 0; (i < 3)&&(!win); i++) {//check horizontal
      win=game[1+i*3]&game[2+i*3]&game[3+i*3];
    }
    for (i = 0; (i < 3)&&(!win); i++) {//check vertical
      win=game[1+i]&game[4+i]&game[7+i];
    }
    if(!win){
      win=game[1]&game[5]&game[9];//check diagonal
      if(!win){
        win=game[3]&game[5]&game[7];//check diagonal2
        if(!win){
          int check=1;
          for (i = 1; (i < 10)&&(check); i++) {
            check=game[i];
          }
          if(check){//check tie
            win=3;
          }
        }
      }
    }
    return win;
  }
  return 0;
}

void endGame(){
  free(game);
}
