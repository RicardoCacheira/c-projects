#include <stdio.h>
#include "ttt.h"

void showGame(){
  printf("\n%d|%d|%d\n-+-+-\n%d|%d|%d\n-+-+-\n%d|%d|%d\n\nPlayer %d: ",
  game[1], game[2], game[3], game[4], game[5], game[6], game[7], game[8], game[9], game[0]);
}

void finalScreen(int n){
  if(n==3){
    printf("\nTie\n");
  }else{
    printf("\nWinner: Player %d\n", n);
  }
}

int main(void) {
  int opc,win=0;
  startGame();
  while (!win) {
    showGame();
    scanf("%d", &opc);
    win=play(opc);
  }
  finalScreen(win);
  endGame();
  return 0;
}
