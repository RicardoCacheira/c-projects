#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#define IMG_WIDTH 31
#define CHAR_WIDTH 7
#define COLOR_0 "\e[2;30;47m"
#define COLOR_1 "\e[2;30;40m"

char * img;

char x_char[]={'#',' ',' ',' ',' ',' ','#',
              ' ','#',' ',' ',' ','#',' ',
              ' ',' ','#',' ','#',' ',' ',
              ' ',' ',' ','#',' ',' ',' ',
              ' ',' ','#',' ','#',' ',' ',
              ' ','#',' ',' ',' ','#',' ',
              '#',' ',' ',' ',' ',' ','#'};

char o_char[]={' ',' ','#','#','#',' ',' ',
              ' ','#',' ',' ',' ','#',' ',
              '#',' ',' ',' ',' ',' ','#',
              '#',' ',' ',' ',' ',' ','#',
              '#',' ',' ',' ',' ',' ','#',
              ' ','#',' ',' ',' ','#',' ',
              ' ',' ','#','#','#',' ',' '};

void startDraw(){
  img=calloc(961,sizeof(char));
  int i;
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i]='#';
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i+310]='#';
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i+620]='#';
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i+930]='#';
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i*31]='#';
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i*31+10]='#';
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i*31+20]='#';
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i*31+30]='#';
  }
}

void writeChar(char *chosen_char,int posX, int posY){
  int i, j;
  for (i = 0; i < CHAR_WIDTH; i++) {
    for (j = 0; j < CHAR_WIDTH; j++) {
      img[posX+j+31*(posY+i)]=chosen_char[j+7*i];
    }
  }

}

void renderGame(){
  struct winsize w;
  ioctl(0, TIOCGWINSZ, &w);
  int alignTop=(w.ws_row-31)/2;
  int alignLeft=(w.ws_col-62)/2;
  int i, j;
  printf(COLOR_0);
  for (i = 0; i < alignTop; i++) {
    printf("\n");
  }
  for (i = 0; i < IMG_WIDTH; i++) {
    for (j = 0; j < alignLeft; j++) {
      printf(" ");
    }
    for (j = 0; j < IMG_WIDTH; j++) {
      if(img[i*IMG_WIDTH+j]=='#'){
        printf(COLOR_0"  ");
      }else{
        printf(COLOR_1"  ");
      }

    }
    printf("\n");
  }
  printf("\n");
  for (j = 0; j < alignLeft; j++) {
    printf(" ");
  }
}

void drawGame(int * game){
  if(game[1]==1){
    writeChar(x_char,2,2);
  }else if(game[1]==2){
    writeChar(o_char,2,2);
  }
  if(game[2]==1){
    writeChar(x_char,12,2);
  }else if(game[2]==2){
    writeChar(o_char,12,2);
  }
  if(game[3]==1){
    writeChar(x_char,22,2);
  }else if(game[3]==2){
    writeChar(o_char,22,2);
  }
  if(game[4]==1){
    writeChar(x_char,2,12);
  }else if(game[4]==2){
    writeChar(o_char,2,12);
  }
  if(game[5]==1){
    writeChar(x_char,12,12);
  }else if(game[5]==2){
    writeChar(o_char,12,12);
  }
  if(game[6]==1){
    writeChar(x_char,22,12);
  }else if(game[6]==2){
    writeChar(o_char,22,12);
  }
  if(game[7]==1){
    writeChar(x_char,2,22);
  }else if(game[7]==2){
    writeChar(o_char,2,22);
  }
  if(game[8]==1){
    writeChar(x_char,12,22);
  }else if(game[8]==2){
    writeChar(o_char,12,22);
  }
  if(game[9]==1){
    writeChar(x_char,22,22);
  }else if(game[9]==2){
    writeChar(o_char,22,22);
  }

  renderGame();

}



void endDraw(){
  free(img);
}
