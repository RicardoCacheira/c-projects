#include <stdio.h>
#include "ttt.h"

void showGame(int player){
  char tab[9];
  int i;
  for (i = 0; i < 9; i++) {
    if(game[i+1]==0){
      tab[i]=' ';
    }else if(game[i+1]==1){
      tab[i]='X';
    }else{
      tab[i]='O';
    }
  }
  printf("\n%c|%c|%c\n-+-+-\n%c|%c|%c\n-+-+-\n%c|%c|%c\n\n",
  tab[0], tab[1], tab[2], tab[3], tab[4], tab[5], tab[6], tab[7], tab[8]);
  if(player){
    printf("Player %d: ", game[0]);
  }
}

int readInput(){//Returns a valid option for the play() funcion, Returns -1 if reads a 'q' or a 'Q'
  char opc[3];
  scanf("%s", opc);
  if(opc[0]=='q'||opc[0]=='Q'){
    return -1;
  }
  int x=opc[0]-'0';
  if(x<10&&x>3){
    return x;
  }
  if(x<4&&x>0){
    int y=opc[1]-'0';
    if(y<4&&y>0){
      return (x-1)*3+y;
    }
    return x;
  }
  return 0;
}

void finalScreen(int n){
  if(n==3){
    printf("Tie\n");
  }else{
    printf("Winner: Player %d\n", n);
  }
}

int main(void) {
  int opc,win=0;
  startGame();
  while (!win) {
    showGame(1);
    opc=readInput();
    if(opc){
      if(opc==-1){
          win=game[0]^3;
      }else{
        win=play(opc);
      }
    }
  }
  showGame(0);
  finalScreen(win);
  endGame();
  return 0;
}
