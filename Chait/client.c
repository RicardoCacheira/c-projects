//       _______   __   __   _______   __   ______
//      / _____/  / /  / /  / ___  /  / /  /_  __/
//     / /       / /__/ /  / /__/ /  / /    / /
//    / /       / ___  /  /  __  /  / /    / /
//   / /____   / /  / /  / /  / /  / /    / /
//  /______/  /_/  /_/  /_/  /_/  /_/    /_/
//
//  Almost as bad as Skype. Almost...
//  Created by Ricardo Cacheira
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include "chait_types.h"
#include <sys/socket.h>
#include <arpa/inet.h>

void printBanner(){
  printf("\e[2J\e[1;1H");
  printf("                     _______   __   __   _______   __   ______\n");
  printf("                    / _____/  / /  / /  / ___  /  / /  /_  __/\n");
  printf("                   / /       / /__/ /  / /__/ /  / /    / /\n");
  printf("                  / /       / ___  /  /  __  /  / /    / /\n");
  printf("                 / /____   / /  / /  / /  / /  / /    / /\n");
  printf("                /______/  /_/  /_/  /_/  /_/  /_/    /_/\n");
  printf("\n                Almost as bad as Skype. Almost...\n");
  printf("                Created by Ricardo Cacheira\n\n");
}

void readServerConfigs(char * validFields, FILE * fp, char * server, int * server_port){
  fgets(server, MESSAGE_TO_SERVER_SIZE, fp);
  server[strlen(server)-1]=0;
  fscanf(fp, "%d%*c", server_port);
  validFields[0]=1;
}

void readUserConfigs(char * validFields, FILE * fp, char * usr, int * client_port){
  fscanf(fp, "%d%*c", client_port);
  fgets(usr, MAX_NAME_SIZE, fp);
  validFields[1]=1;
}

void showHelp(){
  printf("\nDefault files\n\n");
  printf("chait-preferences <- All the definitions (ignores the other files if present)\n");
  printf("chait-connection <- Server definitions\n");
  printf("chait-user <- User defenitions\n\n");
  printf("Possible arguments\n\n");
  printf("-help <- Shows help\n");
  printf("-ignore <- Ignores the default files\n");
  printf("<serverIP> <serverPort> <personalPort> <Username>\n");
  printf("-s <serverIP> <serverPort>\n");
  printf("-u <personalPort> <Username>\n");
  printf("-f <filename with all fields>\n");
  printf("-sf <filename with server fields>\n");
  printf("-uf <filename with user fields>\n");
  printf("-sf <filename with server fields> -uf <filename with user fields>\n");
  printf("-sf <filename with server fields> -u <personalPort> <Username>\n");
  printf("-s <serverIP> <serverPort> -uf <filename with user fields>\n");
  printf("-s <serverIP> <serverPort> -u <personalPort> <Username>\n");
  printf("-s <serverIP> <serverPort>\n");
  exit(0);
}

void loadValues(int argc, char const *argv[], char * validFields, int * client_port, int * server_port, char * usr, char * server){
  FILE * fp;
  char ignore=0;
  if(argc==1){
    fp= fopen("chait-preferences", "r");
    if(fp==NULL){
      fp= fopen("chait-connection", "r");
      if(fp!=NULL){
        readServerConfigs(validFields, fp, server, server_port);
        fclose(fp);
      }
      fp= fopen("chait-user", "r");
      if(fp!=NULL){
        readUserConfigs(validFields, fp, usr, client_port);
        fclose(fp);
      }
    }else{
      readServerConfigs(validFields, fp, server, server_port);
      readUserConfigs(validFields, fp, usr, client_port);
      fclose(fp);
    }
  }else if(argc==2 && strcmp(argv[1],"-help")==0){
    showHelp();
  }else if(argc==2 && strcmp(argv[1],"-ignore")==0){
    ignore=1;
  }else if(argc==5 && argv[1][0]!='-'){
    strcpy(server, argv[1]);
    *server_port=atoi(argv[2]);
    *client_port=atoi(argv[3]);
    strcpy(usr, argv[4]);
    validFields[0]=1;
    validFields[1]=1;
  }else if(argc==3 && strcmp(argv[1],"-f")==0){
      fp=fopen(argv[2], "r");
      readServerConfigs(validFields, fp, server, server_port);
      readUserConfigs(validFields, fp, usr, client_port);
      fclose(fp);
  }else{
    int pointer=1;
    if(argc>=2+pointer && strcmp(argv[pointer],"-sf")==0){
      fp=fopen(argv[pointer+1], "r");
      readServerConfigs(validFields, fp, server, server_port);
      fclose(fp);
      pointer+=2;
    }else if(argc>=3+pointer && strcmp(argv[pointer],"-s")==0){
      strcpy(server, argv[pointer+1]);
      *server_port=atoi(argv[pointer+2]);
      pointer+=3;
      validFields[0]=1;
    }
    if(argc==2+pointer && strcmp(argv[pointer],"-uf")==0){
      fp=fopen(argv[pointer+1], "r");
      readUserConfigs(validFields, fp, usr, client_port);
      fclose(fp);
    }else if(argc==3+pointer && strcmp(argv[pointer],"-u")==0){
      *client_port=atoi(argv[pointer+1]);
      strcpy(usr, argv[pointer+2]);
      validFields[1]=1;
    }
    if(validFields[0]==0 && validFields[1]==0 && !ignore){
      showHelp();
    }
  }
}

int main(int argc, char const *argv[]){
  int client_port, server_port;
  char usr[MAX_NAME_SIZE];
  char server_addr[MESSAGE_TO_SERVER_SIZE];
  char buffer[MESSAGE_TO_SERVER_SIZE];
  char validFields[]={0, 0};
  pid_t pid;
  loadValues(argc, argv, validFields, &client_port, &server_port, usr, server_addr);
  printBanner();
  if(validFields[0]==0){
    printf("Insert desired Server Address: ");
    fgets(server_addr, MESSAGE_TO_SERVER_SIZE, stdin);
    server_addr[strlen(server_addr)-1]=0;
    printf("Insert Server port: ");
    scanf("%d%*c", &server_port);
  }
  if(validFields[1]==0){
    printf("Insert desired client port: ");
    scanf("%d%*c", &client_port);
    printf("Insert Username: ");
    fgets(usr, MAX_NAME_SIZE, stdin);
    usr[strlen(usr)-1]=0;
  }
  struct sockaddr_in client_sockaddr, to_server_sockaddr, from_server_sockaddr;
  int sock,sockaddr_size;
  sock=socket(AF_INET,SOCK_DGRAM,0);
  sockaddr_size=sizeof(client_sockaddr);
  client_sockaddr.sin_family=AF_INET;
  client_sockaddr.sin_addr.s_addr=htonl(INADDR_ANY); /* local IP address */
  client_sockaddr.sin_port=htons(client_port); /*  local Port */
  if(bind(sock,(struct sockaddr *)&client_sockaddr,sockaddr_size)==-1){
    printf("Unable to bind socket.\n");
    close(sock);
    exit(1);
  }
  to_server_sockaddr.sin_family=AF_INET;
  to_server_sockaddr.sin_addr.s_addr=inet_addr(server_addr);
  to_server_sockaddr.sin_port=htons(server_port); /* server port */
  strcpy(buffer+1, usr);
  buffer[0]=REGISTER_TYPE;
  sendto(sock,buffer,strlen(buffer),0,(struct sockaddr *)&to_server_sockaddr,sockaddr_size);
  int rec=recvfrom(sock,buffer,2,0,(struct sockaddr *)&from_server_sockaddr, &sockaddr_size);
  if(buffer[1]==ACCEPT_REGISTRY){
    printBanner();
    printf("  Connected to %s:%d\n\n  Welcome %s\n  ______________________________________Chat__________________________________\n\n", server_addr, server_port, usr);
    setbuf(stdout, NULL);
    pid=fork();
    if(pid==0){
      printf("  ____________________________________________________________________________\n\n  Message: ");
      do{
        //TODO Send private message to user
        fgets(buffer+1, MAX_MESSAGE_SIZE, stdin);
        printf("\e[1A");
        if(buffer[1]=='/'){
          if(buffer[2]==QUIT_TAG){
            buffer[0]=DISCONNECT_TYPE;
            sendto(sock,buffer,1,0,(struct sockaddr *)&to_server_sockaddr,sockaddr_size);
            exit(0);
          }else if(buffer[2]==ADMIN_COMMAND_TAG){
            buffer[0]=ADMIN_COMMAND_TYPE;
            sendto(sock,buffer+2,strlen(buffer+2)-1,0,(struct sockaddr *)&to_server_sockaddr,sockaddr_size);
          }
        }else{
          buffer[0]=CHAT_TYPE;
          sendto(sock,buffer,strlen(buffer)-1,0,(struct sockaddr *)&to_server_sockaddr,sockaddr_size);
        }

      }while(1);
    }else{
      char from_server_buffer[MESSAGE_FROM_SERVER_SIZE];
      char continue_flag=1;
      do{
        rec=recvfrom(sock,from_server_buffer,MESSAGE_FROM_SERVER_SIZE,0,(struct sockaddr *)&from_server_sockaddr, &sockaddr_size);
        switch (from_server_buffer[0]) {
          case CHAT_TYPE:
            printf("\e[2K\e[2A\e[1G\e[2K  %s\n  ____________________________________________________________________________\n\n  Message: ", from_server_buffer+1);
            break;
          case DISCONNECT_TYPE:
            printf("\e[2J\e[1;1H%s\n", from_server_buffer+1);
            continue_flag=0;
            break;
        }
      }while(continue_flag);
    }
  }else if(buffer[1]==DECLINE_REGISTRY){
    printf("Unable to register on server.\n");
  }else{
    printf("Invalid Registry Request Answer.\n");
  }
  kill(pid, SIGTERM);
  close(sock);
  return 0;
}
