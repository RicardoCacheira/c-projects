//       _______   __   __   _______   __   ______
//      / _____/  / /  / /  / ___  /  / /  /_  __/
//     / /       / /__/ /  / /__/ /  / /    / /
//    / /       / ___  /  /  __  /  / /    / /
//   / /____   / /  / /  / /  / /  / /    / /
//  /______/  /_/  /_/  /_/  /_/  /_/    /_/
//
//  Almost as bad as Skype. Almost...
//  Created by Ricardo Cacheira
#include <netinet/in.h>

#define MAX_MESSAGE_SIZE 256
#define MAX_ADDR_SIZE 128
#define REGISTER_TYPE 'r'
#define CHAT_TYPE 'c'
#define PRIVATE_CHAT_TYPE 'p'
#define DISCONNECT_TYPE 'd'
#define ADMIN_COMMAND_TYPE 'a'
#define ACCEPT_REGISTRY 'y'
#define DECLINE_REGISTRY 'n'
#define ADMIN_COMMAND_TAG 'a'
#define QUIT_TAG 'q'
#define MAX_NAME_SIZE 64
#define MAX_DEFAULT_USER_SIZE 16
#define MAX_PASSAWORD_SIZE 64
#define DEFAULT_SERVER_NAME "Server"
#define DEFAULT_ADMIN_PASS "admin123"
#define MESSAGE_TO_SERVER_SIZE 1+MAX_MESSAGE_SIZE //full message->type+message
#define MESSAGE_FROM_SERVER_SIZE 1+MAX_NAME_SIZE+MAX_MESSAGE_SIZE+3 //full message->type+'<'+sender name+"> "+message

typedef struct{
  char active;
  char username[MAX_NAME_SIZE];
  struct in_addr address;
  u_short port;
}user_reg;
