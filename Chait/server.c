//       _______   __   __   _______   __   ______
//      / _____/  / /  / /  / ___  /  / /  /_  __/
//     / /       / /__/ /  / /__/ /  / /    / /
//    / /       / ___  /  /  __  /  / /    / /
//   / /____   / /  / /  / /  / /  / /    / /
//  /______/  /_/  /_/  /_/  /_/  /_/    /_/
//
//  Almost as bad as Skype. Almost...
//  Created by Ricardo Cacheira
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include "chait_types.h"
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

time_t current_time;
char* c_time_string;
struct tm * timeinfo;
int sock;
struct sockaddr_in to_client_sockaddr;
int sockaddr_size;
user_reg database[MAX_DEFAULT_USER_SIZE];
char pass[MAX_PASSAWORD_SIZE];
char continue_flag;

void printBanner(){
  printf("\e[2J\e[1;1H\n");
  printf("       _______   __   __   _______   __   ______\n");
  printf("      / _____/  / /  / /  / ___  /  / /  /_  __/\n");
  printf("     / /       / /__/ /  / /__/ /  / /    / /\n");
  printf("    / /       / ___  /  /  __  /  / /    / /\n");
  printf("   / /____   / /  / /  / /  / /  / /    / /\n");
  printf("  /______/  /_/  /_/  /_/  /_/  /_/    /_/\n");
  printf("\n  Almost as bad as Skype. Almost...\n");
  printf("  Created by Ricardo Cacheira\n\n");
}

void printTime(){
  current_time = time(NULL);
  c_time_string = ctime(&current_time);
  printf("\n%s", c_time_string);
}

void sendMessage2(char * msn, int msn_size, struct in_addr address, u_short port){
  to_client_sockaddr.sin_addr.s_addr=address.s_addr;
  to_client_sockaddr.sin_port=port;
  sendto(sock,msn,msn_size,0,(struct sockaddr *)&to_client_sockaddr,sockaddr_size);
}

void sendMessage(char * msn, int msn_size, user_reg user){
  sendMessage2(msn, msn_size, user.address, user.port);
}

void sendMessageToUser(char * msn, int msn_size, char * username){
  int i;
  char found=0;
  for ( i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
    if(strcmp(username, database[i].username)==0){
      found=1;
      break;
    }
  }
  if(found){
    sendMessage(msn, msn_size, database[i]);
  }

}

void sendBroadcastMessage(char * msn){
  int i;
  for ( i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
    if(database[i].active){
      sendMessage(msn, strlen(msn)+1, database[i]);

    }
  }
}

void registerUser(struct sockaddr_in * from_client_sockaddr, char * register_str){
    char msn[2];
    msn[0]=REGISTER_TYPE;
    char valid=1;
    int pos=-1;
    int i;
    for ( i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
      if(strcmp(database[i].username, register_str)==0 && database[i].active){
        valid=0;
        break;
      }
      if(pos==-1 && (!database[i].active)){
        pos=i;
      }
    }
    if(valid && pos!=-1){
      char aux[MESSAGE_FROM_SERVER_SIZE];
      sprintf(aux, "%cUser \"%s\" has connected to the server.", CHAT_TYPE, register_str);
      sendBroadcastMessage(aux);
      database[pos].active=1;
      strcpy(database[pos].username, register_str);
      database[pos].address.s_addr=from_client_sockaddr->sin_addr.s_addr;
      database[pos].port=from_client_sockaddr->sin_port;
      msn[1]=ACCEPT_REGISTRY;
      printf("Accepted.\nUsername:%s\nAddress: %s\nPort: %hu\n", database[pos].username, inet_ntoa(from_client_sockaddr->sin_addr), ntohs(from_client_sockaddr->sin_port));
    }else{
      msn[1]=DECLINE_REGISTRY;
      printf("Declined.\n");
    }
    sendMessage2(msn, 2, from_client_sockaddr->sin_addr, from_client_sockaddr->sin_port);
}

char * getUsername(struct sockaddr_in * from_client_sockaddr){
  int i;
  for ( i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
    if(database[i].address.s_addr==from_client_sockaddr->sin_addr.s_addr && database[i].port==from_client_sockaddr->sin_port && database[i].active){
      return database[i].username;
    }
  }
}

void buildChatMessage(char * final, char * username, char * msn){
  final[0]=CHAT_TYPE;
  final[1]='<';
  strcpy(final+2, username);
  int aux=strlen(username);
  final[aux+2]='>';
  final[aux+3]=' ';
  strcpy(final+aux+4,msn);
}

void sendChat(struct sockaddr_in * from_client_sockaddr, char * msn){
  char final[MESSAGE_FROM_SERVER_SIZE];
  buildChatMessage(final, getUsername(from_client_sockaddr), msn);
  printf("Chat sent by %s\n", final+1);
  sendBroadcastMessage(final);
}

void disconnectUser(struct sockaddr_in * from_client_sockaddr){
  char msn[MESSAGE_FROM_SERVER_SIZE];
  char * user=getUsername(from_client_sockaddr);
  sprintf(msn,"%cYou have been disconnected.", DISCONNECT_TYPE);
  sendMessage2(msn, strlen(msn)+1, from_client_sockaddr->sin_addr, from_client_sockaddr->sin_port);
  int i;
  for ( i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
    if(strcmp(database[i].username, user)==0 && database[i].active){
      database[i].active=0;
      break;
    }
  }
  sprintf(msn,"%cUser \"%s\" has disconnected", CHAT_TYPE, user);
  printf("%s\n", msn+1);
  sendBroadcastMessage(msn);
}

void kickUser(char * username){
  char msn[MESSAGE_FROM_SERVER_SIZE];
  char found=0;
  int i;
  for ( i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
    if(strcmp(database[i].username, username)==0 && database[i].active){
      database[i].active=0;
      found=1;
      break;
    }
  }
  if(found){
    sprintf(msn,"%cYou have been kicked from the server.", DISCONNECT_TYPE);
    sendMessage2(msn, strlen(msn)+1, database[i].address, database[i].port);
    sprintf(msn,"%cUser \"%s\" has been kicked from the server.", CHAT_TYPE, username);
    printf("%s\n", msn+1);
    sendBroadcastMessage(msn);
  }
}

void runAdminCommand(struct sockaddr_in * user_sockaddr, struct sockaddr_in * server_sockaddr, char *msn){
  char command[6];
  int i;
  char aux[MESSAGE_FROM_SERVER_SIZE];
  command[5]=0;
  for ( i = 0; i < 5; i++) {
    command[i]=msn[i];
  }
  printf("command sent from %s:\n%s\n", getUsername(user_sockaddr), msn);
  if(strcmp(command,"USERS")==0){
    printf("User list:\n");
    sprintf(aux, "User list\n");
    sendMessage2(aux, strlen(aux)+1, user_sockaddr->sin_addr, user_sockaddr->sin_port);
    int i;
    for ( i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
      if(database[i].active){
        printf("Username: %s\nAddress: %s\nPort: %d\n\n", database[i].username, inet_ntoa(database[i].address), ntohs(database[i].port));
        sprintf(aux, "%cUsername: %s\n  Address: %s\n  Port: %d\n", CHAT_TYPE, database[i].username, inet_ntoa(database[i].address), ntohs(database[i].port));
        sendMessage2(aux, strlen(aux)+1, user_sockaddr->sin_addr, user_sockaddr->sin_port);
      }
    }
  }else if(strcmp(command,"SENDU")==0){
    //TODO Send messages for server to an user
  }else{
    command[4]=0;
    if(strcmp(command,"EXIT")==0){
      sprintf(aux, "%cServer has shutdown.",DISCONNECT_TYPE);
      sendBroadcastMessage(aux);
      continue_flag=0;
    }else if(strcmp(command,"KICK")==0){
      kickUser(msn+5);
    }else if(strcmp(command,"SEND")==0){
      sprintf(aux, "%cServer says: %s",CHAT_TYPE, msn+5);
      sendBroadcastMessage(aux);
    }else if(strcmp(command,"PORT")==0){
      sprintf(aux, "%cServer Port:%d",CHAT_TYPE, ntohs(server_sockaddr->sin_port));
      printf("Server Port:%d\n", ntohs(server_sockaddr->sin_port));
      sendMessage2(aux, strlen(aux)+1, user_sockaddr->sin_addr, user_sockaddr->sin_port);
    }else{
      sprintf(aux, "%cInvalid command.",CHAT_TYPE);
      printf("Invalid command.\n");
      sendMessage2(aux, strlen(aux)+1, user_sockaddr->sin_addr, user_sockaddr->sin_port);
    }
  }
}

int main(void){
  strcpy(pass,DEFAULT_ADMIN_PASS);
  printBanner();
  timeinfo = localtime (&current_time);
  //----------<Socket code>---------//
  int server_port;
  printf("Insert desired port: ");
  scanf("%d", &server_port);
  struct sockaddr_in server_sockaddr, from_client_sockaddr;
  int rec;
  char buffer[MESSAGE_TO_SERVER_SIZE];
  sock=socket(AF_INET,SOCK_DGRAM,0);
  sockaddr_size=sizeof(server_sockaddr);
  server_sockaddr.sin_family=AF_INET;
  server_sockaddr.sin_addr.s_addr=htonl(INADDR_ANY); /* local IP address */
  server_sockaddr.sin_port=htons(server_port); /* local Port */
  if(bind(sock,(struct sockaddr *)&server_sockaddr,sockaddr_size)==-1){
    printf("Unable to bind socket.\n");
    close(sock);
    exit(1);
  }
  printf("Server Port: %d\n\n--Log--\n", server_port);
  to_client_sockaddr.sin_family=AF_INET;
  int i;
  for (i = 0; i < MAX_DEFAULT_USER_SIZE; i++) {
    database[i].active=0;
  }
  //----------</Socket code>---------//
  //----------<Receive code>---------//
  continue_flag=1;
  do{
    rec=recvfrom(sock,buffer,MESSAGE_TO_SERVER_SIZE,0,(struct sockaddr *)&from_client_sockaddr, &sockaddr_size);
    buffer[rec]=0;
    printTime();
    int pass_size=strlen(pass);
    int valid=1;
    switch(buffer[0]){
      case CHAT_TYPE:
        sendChat(&from_client_sockaddr, buffer+1);
        break;
      case REGISTER_TYPE:
        registerUser(&from_client_sockaddr, buffer+1);
        break;
      case DISCONNECT_TYPE:
        disconnectUser(&from_client_sockaddr);
        break;
      case ADMIN_COMMAND_TYPE:
        for (i = 0; i < pass_size; i++) {
          if(buffer[1+i]!=pass[i]){
            valid=0;
            break;
          }
        }
        if(valid){
          runAdminCommand(&from_client_sockaddr, &server_sockaddr, buffer+2+pass_size);
        }else{
          printf("Invalid acess to admin commands by user \"%s\".\n", getUsername(&from_client_sockaddr));
          char aux[MESSAGE_FROM_SERVER_SIZE];
          sprintf(aux, "%cAdmin command Denied", CHAT_TYPE);
          sendMessage2(aux, strlen(aux)+1, from_client_sockaddr.sin_addr, from_client_sockaddr.sin_port);
        }
        break;
      default:
        printf("Undefined type of message (Type: %c).\nMessage: %s", buffer[0], buffer+1);
        break;
    }
  }while(continue_flag);
  //----------</Receive code>---------//
  close(sock);
  return 0;
}
