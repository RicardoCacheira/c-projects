#include <stdio.h>
#include "TrafficLights.h"
#include "drawTrafficLights.h"

void showGame(int player){
  drawGame(game);
  if(player){
    printf("                           Player %d: ", game[0]);
  }else{
    printf("                     The Winner is Player %d!\n", game[0]^3);
  }
}

int readInput(){//Returns a valid option for the play() funcion, Returns -1 if reads a 'q' or a 'Q'
  char opc[3];
  scanf("%s", opc);
  if(opc[0]=='q'||opc[0]=='Q'){
    return -1;
  }
  int x=opc[0]-'0';
  if(x<4&&x>0){
    int y=opc[1]-'0';
    if(y<5&&y>0){
      return (x-1)*4+y;
    }
  }
  return 0;
}

int main(void) {
  startDraw();
  int opc,win=0;
  startGame();
  while (!win) {
    showGame(1);
    opc=readInput();
    if(opc){
      if(opc==-1){
          win=game[0]^3;
      }else{
        win=play(opc);
      }
    }
  }
  showGame(0);
  endGame();
  return 0;
}
