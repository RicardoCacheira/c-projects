#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#define IMG_WIDTH 33
#define IMG_HEIGHT 25
#define CHAR_WIDTH 7
#define RESET "\e[0m"
#define COLOR_TABLE "\e[2;30;47m"
#define COLOR_NONE "\e[2;30;40m"
#define COLOR_GREEN "\e[2;30;42m"
#define COLOR_YELLOW "\e[2;30;43m"
#define COLOR_RED "\e[2;30;41m"

char * img;

void startDraw(){
  img=calloc(IMG_WIDTH*IMG_HEIGHT,sizeof(char));
  int i;
  for (i = 0; i < IMG_WIDTH; i++) {
    img[i]=3;
    img[i+IMG_WIDTH*(CHAR_WIDTH+1)]=3;
    img[i+IMG_WIDTH*(CHAR_WIDTH+1)*2]=3;
    img[i+IMG_WIDTH*(CHAR_WIDTH+1)*3]=3;
  }
  for (i = 0; i < IMG_HEIGHT; i++) {
    img[0+IMG_WIDTH*i]=3;
    img[CHAR_WIDTH+1+IMG_WIDTH*i]=3;
    img[(CHAR_WIDTH+1)*2+IMG_WIDTH*i]=3;
    img[(CHAR_WIDTH+1)*3+IMG_WIDTH*i]=3;
    img[(CHAR_WIDTH+1)*4+IMG_WIDTH*i]=3;
  }
}

void writeChar(char chosen_char,int posX, int posY){
  int i, j;
  for (i = 0; i < CHAR_WIDTH; i++) {
    for (j = 0; j < CHAR_WIDTH; j++) {
      img[posX+j+IMG_WIDTH*(posY+i)]=chosen_char;
    }
  }

}

void renderGame(){
  struct winsize w;
  ioctl(0, TIOCGWINSZ, &w);
  int alignTop=(w.ws_row-IMG_HEIGHT)/2;
  int alignLeft=(w.ws_col-IMG_WIDTH*2)/2;
  int i, j;
  //printf(COLOR_TABLE);
  for (i = 0; i < alignTop; i++) {
    printf("\n");
  }
  for (i = 0; i < IMG_HEIGHT; i++) {
    for (j = 0; j < alignLeft; j++) {
      printf(" ");
    }
    for (j = 0; j < IMG_WIDTH; j++) {
      if(img[i*IMG_WIDTH+j]==0){
        printf(RESET"  ");
      }else if(img[i*IMG_WIDTH+j]==1){
        printf(COLOR_GREEN"  ");
      }else if(img[i*IMG_WIDTH+j]==2){
        printf(COLOR_YELLOW"  ");
      }else if(img[i*IMG_WIDTH+j]==4){
        printf(COLOR_RED"  ");
      }else{
        printf(COLOR_TABLE"  ");
      }

    }
    printf(RESET"\n");
  }
  for (i = 0; i < alignTop; i++) {
    printf("\n");
  }
  for (j = 0; j < alignLeft; j++) {
    printf(" ");
  }
}

void drawGame(int * game){
  writeChar(game[1],1,1);
  writeChar(game[2],9,1);
  writeChar(game[3],17,1);
  writeChar(game[4],25,1);
  writeChar(game[5],1,9);
  writeChar(game[6],9,9);
  writeChar(game[7],17,9);
  writeChar(game[8],25,9);
  writeChar(game[9],1,17);
  writeChar(game[10],9,17);
  writeChar(game[11],17,17);
  writeChar(game[12],25,17);

  renderGame();

}



void endDraw(){
  free(img);
}
