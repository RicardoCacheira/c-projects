#include <stdio.h>
#include <stdlib.h>
#include "TrafficLights.h"
#define IMG_WIDTH 33
#define IMG_HEIGHT 25
#define CHAR_WIDTH 7

void showGame(){
  printf("\n%d|%d|%d|%d\n-+-+-+-\n%d|%d|%d|%d\n-+-+-+-\n%d|%d|%d|%d\n\nPlayer %d: ",
  game[1], game[2], game[3], game[4], game[5], game[6], game[7], game[8], game[9], game[10], game[11], game[12], game[0]);
}

int main(void) {
  int opc,win=0;
  startGame();
  while (!win) {
    showGame();
    scanf("%d", &opc);
    win=play(opc);
  }
  printf("\nWinner: Player %d\n", game[0]^3);
  endGame();
  return 0;
}
