#include <stdlib.h>

int * game;
void startGame(){
  game=calloc(13,sizeof(int));
  game[0]=1;
}

int play(opc){
  if(game[opc]!=4){
    if(!game[opc]){
      game[opc]=1;
    }
    else{
      game[opc]=game[opc]<<1;
    }
    game[0]=game[0]^3;//Change player (1^3=2, 2^3=1)
    int i,win=0;
    for (i = 0; (i < 3)&&(!win); i++) {//check horizontal
      win=game[1+i*4]&game[2+i*4]&game[3+i*4];
    }
    for (i = 0; (i < 3)&&(!win); i++) {//check horizontal2
      win=game[2+i*4]&game[3+i*4]&game[4+i*4];
    }
    for (i = 0; (i < 3)&&(!win); i++) {//check vertical
      win=game[1+i]&game[5+i]&game[9+i];
    }
    if(!win){
      win=game[1]&game[6]&game[11];//check diagonal
      if(!win){
        win=game[2]&game[7]&game[12];//check diagonal2
        if(!win){
          win=game[3]&game[6]&game[9];//check diagonal3
          if(!win){
            win=game[4]&game[7]&game[10];//check diagonal4
          }
        }
      }
    }
    return win;
  }
  return 0;
}

void endGame(){
  free(game);
}
