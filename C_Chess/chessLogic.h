#include <stdlib.h>
#include "chessTable.h"
#define ROW_LENGHT 8
#define MAX_NUMBER_POSSIBLE_MOVES 27

chessTable * startGame();

chessTable * startCustomGame(char * gameStr);

void play(chessTable * board, int numMoves, chessMove * moves, chessMove moveTo);

int getPossibleValidMoves(chessTable * board, chessMove * moves);

void endGame(chessTable * board);
