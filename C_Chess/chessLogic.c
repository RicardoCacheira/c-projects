#include <stdlib.h>
#include <stdio.h>
#include "chessTable.h"
#include "getch.h"

#define ROW_LENGHT 8
#define MAX_NUMBER_POSSIBLE_MOVES 27

chessTable * startGame(){
  int i;
  chessTable * board=calloc(1,sizeof(chessTable));
  board->table=calloc(64,sizeof(chessPiece));
  //Defining the Black pieces
  board->table[0*ROW_LENGHT+0].pieceType=ROOK_TYPE;
  board->table[0*ROW_LENGHT+1].pieceType=KNIGHT_TYPE;
  board->table[0*ROW_LENGHT+2].pieceType=BISHOP_TYPE;
  board->table[0*ROW_LENGHT+3].pieceType=QUEEN_TYPE;
  board->table[0*ROW_LENGHT+4].pieceType=KING_TYPE;
  board->table[0*ROW_LENGHT+5].pieceType=BISHOP_TYPE;
  board->table[0*ROW_LENGHT+6].pieceType=KNIGHT_TYPE;
  board->table[0*ROW_LENGHT+7].pieceType=ROOK_TYPE;
  //Defining the White pieces
  board->table[7*ROW_LENGHT+0].pieceType=ROOK_TYPE;
  board->table[7*ROW_LENGHT+1].pieceType=KNIGHT_TYPE;
  board->table[7*ROW_LENGHT+2].pieceType=BISHOP_TYPE;
  board->table[7*ROW_LENGHT+3].pieceType=QUEEN_TYPE;
  board->table[7*ROW_LENGHT+4].pieceType=KING_TYPE;
  board->table[7*ROW_LENGHT+5].pieceType=BISHOP_TYPE;
  board->table[7*ROW_LENGHT+6].pieceType=KNIGHT_TYPE;
  board->table[7*ROW_LENGHT+7].pieceType=ROOK_TYPE;
  //Defining the pawns and the pieces color
  for (i = 0; i < ROW_LENGHT; i++) {
    board->table[1*ROW_LENGHT+i].pieceType=PAWN_TYPE;
    board->table[6*ROW_LENGHT+i].pieceType=PAWN_TYPE;

    board->table[0*ROW_LENGHT+i].pieceColor=BLACK_PIECE_COLOR;
    board->table[1*ROW_LENGHT+i].pieceColor=BLACK_PIECE_COLOR;
    board->table[6*ROW_LENGHT+i].pieceColor=WHITE_PIECE_COLOR;
    board->table[7*ROW_LENGHT+i].pieceColor=WHITE_PIECE_COLOR;
  }
  board->activePlayer=1;
  return board;
}

chessTable * startCustomGame(char * gameStr){
  chessTable * board=calloc(1,sizeof(chessTable));
  board->table=calloc(64,sizeof(chessPiece));
  chessPiece * aux =board->table;
  int i=0;
  while(*gameStr!=0){
    char auxPlayer =*gameStr;
    printf("i: %d, player: %c", i, auxPlayer);
    if(auxPlayer=='1'||auxPlayer=='2'){
      gameStr++;
      aux->pieceColor=auxPlayer-'0';
      aux->pieceType=*gameStr;
      printf(", %c", *gameStr);
    }
    printf("\n");
    gameStr++;
    i++;
    aux++;
  }
  board->activePlayer=1;
  return board;
}

void endGame(chessTable * board){
  free(board->table);
  free(board);
}

int getInactivePlayer(chessTable * board){
  return board->activePlayer^3;
}

void switchPlayer(chessTable * board){
  board->activePlayer=getInactivePlayer(board);
}


void clearPieceInPosition(chessTable * board, chessMove position){
  chessPiece * piece = &(board->table[position.posY*ROW_LENGHT+position.posX]);
  piece->pieceColor=0;
  piece->pieceType=0;
  piece->specialFlag=0;
}
//TODO check the specialFlag before que en passant or castling check and create an make special move function
void movePiece(chessTable * board, chessMove from, chessMove to){
  chessPiece * fromPiece = &board->table[from.posY*ROW_LENGHT+from.posX];
  chessPiece * toPiece = &board->table[to.posY*ROW_LENGHT+to.posX];
  //Code for the en passant move
  if(fromPiece->pieceType==PAWN_TYPE){
    //In case of the white pawn moving 2 ranks
    if(to.posY-from.posY==-2){
      board->possibleEnPassant=1;
      board->enPassantMove.posX=from.posX;
      board->enPassantMove.posY=from.posY-1;
    }
    //In case of the black pawn moving 2 ranks
    else if(to.posY-from.posY==2){
      board->possibleEnPassant=1;
      board->enPassantMove.posX=from.posX;
      board->enPassantMove.posY=from.posY+1;

    }//If an en passant Move was made
    else if(board->possibleEnPassant && to.posX==board->enPassantMove.posX && to.posY==board->enPassantMove.posY){
      chessMove pieceInEnPassant;
      pieceInEnPassant.posX=board->enPassantMove.posX;
      if(fromPiece->pieceColor==WHITE_PIECE_COLOR){
        pieceInEnPassant.posY=board->enPassantMove.posY+1;
      }else{
        pieceInEnPassant.posY=board->enPassantMove.posY-1;
      }
      clearPieceInPosition(board, pieceInEnPassant);
    }else if(board->possibleEnPassant){
      board->possibleEnPassant=0;
    }

  }else{
    //Deactivate the possible en Passant flag
    if(board->possibleEnPassant){
      board->possibleEnPassant=0;
    }
    //In case of an castling
    if(fromPiece->pieceType==KING_TYPE && fromPiece->specialFlag==0){
      if(to.posX==2){
        chessMove rookPosition, castlingMove;
        rookPosition.posY=from.posY;
        castlingMove.posY=from.posY;
        rookPosition.posX=0;
        castlingMove.posX=3;
        movePiece(board, rookPosition, castlingMove);
      }else if(to.posX==6){
        chessMove rookPosition, castlingMove;
        rookPosition.posY=from.posY;
        castlingMove.posY=from.posY;
        rookPosition.posX=7;
        castlingMove.posX=5;
        movePiece(board, rookPosition, castlingMove);
      }
    }
  }
  toPiece->pieceColor=fromPiece->pieceColor;
  toPiece->pieceType=fromPiece->pieceType;
  toPiece->specialFlag=1;
  clearPieceInPosition(board, from);
}

int movesAreEqual(chessMove a, chessMove b){
  if(a.posX==b.posX && a.posY==b.posY){
    return 1;
  }
  return 0;
}

int checkKingMoves(chessTable * board, chessPiece * piece, chessMove * moves){
  int num=0;
  int leftValid=moves[0].posX!=0;
  int rightValid=moves[0].posX!=7;
  int topValid=moves[0].posY!=0;
  int bottomValid=moves[0].posY!=7;
  int x,y;
  if(topValid){
    x=moves[0].posX;
    y=moves[0].posY-1;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
    if(leftValid){
      x=moves[0].posX-1;
      y=moves[0].posY-1;
      if(board->table[y*8+x].pieceColor!=piece->pieceColor){
        num++;
        moves[num].posX=x;
        moves[num].posY=y;
      }
    }
    if(rightValid){
      x=moves[0].posX+1;
      y=moves[0].posY-1;
      if(board->table[y*8+x].pieceColor!=piece->pieceColor){
        num++;
        moves[num].posX=x;
        moves[num].posY=y;
      }
    }
  }
  if(bottomValid){
    x=moves[0].posX;
    y=moves[0].posY+1;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
    if(leftValid){
      x=moves[0].posX-1;
      y=moves[0].posY+1;
      if(board->table[y*8+x].pieceColor!=piece->pieceColor){
        num++;
        moves[num].posX=x;
        moves[num].posY=y;
      }
    }
    if(rightValid){
      x=moves[0].posX+1;
      y=moves[0].posY+1;
      if(board->table[y*8+x].pieceColor!=piece->pieceColor){
        num++;
        moves[num].posX=x;
        moves[num].posY=y;
      }
    }
  }
  if(leftValid){
    x=moves[0].posX-1;
    y=moves[0].posY;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(rightValid){
    x=moves[0].posX+1;
    y=moves[0].posY;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(piece->specialFlag==0){
    chessPiece possibleRook=board->table[moves[0].posY*8];
    //Check if the leftmost piece is a rook of the same color that wasn't moved
    if(possibleRook.pieceColor==piece->pieceColor && possibleRook.specialFlag==0 && possibleRook.pieceType==ROOK_TYPE){
      //Check if the space between the rook and the king are free
      if(board->table[moves[0].posY*8+1].pieceColor==0 && board->table[moves[0].posY*8+2].pieceColor==0 && board->table[moves[0].posY*8+3].pieceColor==0){
        //TODO Find a way to check if the king does not pass through a square that is attacked by an enemy piece
        num++;
        moves[num].posX=moves[0].posX-2;
        moves[num].posY=moves[0].posY;
      }
    }
    possibleRook=board->table[moves[0].posY*8+7];
    //Check if the leftmost piece is a rook of the same color that wasn't moved
    if(possibleRook.pieceColor==piece->pieceColor && possibleRook.specialFlag==0 && possibleRook.pieceType==ROOK_TYPE){
      //Check if the space between the rook and the king are free
      if(board->table[moves[0].posY*8+5].pieceColor==0 && board->table[moves[0].posY*8+6].pieceColor==0){
      num++;
      moves[num].posX=moves[0].posX+2;
      moves[num].posY=moves[0].posY;
      }
    }
  }
  return num;
}

int checkRookMoves(chessTable * board, chessPiece * piece, chessMove * moves){
  int num=0;
  int i;
  //Check positions to the right
  for (i = moves[0].posX+1; i < 8; i++) {
    int colorToCheck=board->table[(moves[0].posY)*8+i].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=i;
      moves[num].posY=moves[0].posY;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=i;
        moves[num].posY=moves[0].posY;
      }
      break;
    }
  }
  //Check positions to the left
  for (i = moves[0].posX-1; i >= 0; i--) {
    int colorToCheck=board->table[(moves[0].posY)*8+i].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=i;
      moves[num].posY=moves[0].posY;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=i;
        moves[num].posY=moves[0].posY;
      }
      break;
    }
  }
  //Check positions down
  for (i = moves[0].posY+1; i < 8; i++) {
    int colorToCheck=board->table[i*8+moves[0].posX].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=moves[0].posX;
      moves[num].posY=i;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=moves[0].posX;
        moves[num].posY=i;
      }
      break;
    }
  }
  //Check positions up
  for (i = moves[0].posY-1; i >= 0; i--) {
    int colorToCheck=board->table[i*8+moves[0].posX].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=moves[0].posX;
      moves[num].posY=i;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=moves[0].posX;
        moves[num].posY=i;
      }
      break;
    }
  }
  return num;
}

int checkBishopMoves(chessTable * board, chessPiece * piece, chessMove * moves){
  int num=0;
  int i;
  //Check positions to the down-right
  for (i = 1; i < 8-moves[0].posX; i++) {
    int yPos=moves[0].posY+i;
    if(yPos>=8){
      break;
    }
    int colorToCheck=board->table[yPos*8+moves[0].posX+i].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=moves[0].posX+i;
      moves[num].posY=yPos;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=moves[0].posX+i;
        moves[num].posY=yPos;
      }
      break;
    }
  }
  //Check positions to the up-right
  for (i = 1; i < 8-moves[0].posX; i++) {
    int yPos=moves[0].posY-i;
    if(yPos<0){
      break;
    }
    int colorToCheck=board->table[yPos*8+moves[0].posX+i].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=moves[0].posX+i;
      moves[num].posY=yPos;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=moves[0].posX+i;
        moves[num].posY=yPos;
      }
      break;
    }
  }
  //Check positions to the down-left
  for (i = 1; i <=moves[0].posX ; i++) {
    int yPos=moves[0].posY+i;
    if(yPos>=8){
      break;
    }
    int colorToCheck=board->table[yPos*8+moves[0].posX-i].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=moves[0].posX-i;
      moves[num].posY=yPos;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=moves[0].posX-i;
        moves[num].posY=yPos;
      }
      break;
    }
  }
  //Check positions to the down-right
  for (i = 1; i <= moves[0].posX; i++) {
    int yPos=moves[0].posY-i;
    if(yPos<0){
      break;
    }
    int colorToCheck=board->table[yPos*8+moves[0].posX-i].pieceColor;
    if(colorToCheck==0){
      num++;
      moves[num].posX=moves[0].posX-i;
      moves[num].posY=yPos;
    }else{
      if(colorToCheck==(piece->pieceColor^3)){
        num++;
        moves[num].posX=moves[0].posX-i;
        moves[num].posY=yPos;
      }
      break;
    }
  }
  return num;
}

int checkQueenMoves(chessTable * board, chessPiece * piece, chessMove * moves){
  int i;
  chessMove tempMoves[MAX_NUMBER_POSSIBLE_MOVES];
  tempMoves[0].posX=moves[0].posX;
  tempMoves[0].posY=moves[0].posY;
  int tempNumMoves=checkBishopMoves(board, piece, tempMoves);
  int numMoves=checkRookMoves(board, piece, moves);
  tempNumMoves+=numMoves;
  for ( i = numMoves+1; i <= tempNumMoves; i++) {
    moves[i].posX=tempMoves[i-numMoves].posX;
    moves[i].posY=tempMoves[i-numMoves].posY;
  }
  return tempNumMoves;
}

int checkKnightMoves(chessTable * board, chessPiece * piece, chessMove * moves){
  int num=0;
  int x,y;
  int leftOnePos=moves[0].posX>0;
  int leftTwoPos=moves[0].posX>1;
  int rightOnePos=moves[0].posX<7;
  int rightTwoPos=moves[0].posX<6;
  int topOnePos=moves[0].posY>0;
  int topTwoPos=moves[0].posY>1;
  int bottomOnePos=moves[0].posY<7;
  int bottomTwoPos=moves[0].posY<6;
  if(topTwoPos&&leftOnePos){
    x=moves[0].posX-1;
    y=moves[0].posY-2;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(topTwoPos&&rightOnePos){
    x=moves[0].posX+1;
    y=moves[0].posY-2;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(topOnePos&&leftTwoPos){
    x=moves[0].posX-2;
    y=moves[0].posY-1;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(topOnePos&&rightTwoPos){
    x=moves[0].posX+2;
    y=moves[0].posY-1;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(bottomTwoPos&&leftOnePos){
    x=moves[0].posX-1;
    y=moves[0].posY+2;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(bottomTwoPos&&rightOnePos){
    x=moves[0].posX+1;
    y=moves[0].posY+2;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(bottomOnePos&&leftTwoPos){
    x=moves[0].posX-2;
    y=moves[0].posY+1;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  if(bottomOnePos&&rightTwoPos){
    x=moves[0].posX+2;
    y=moves[0].posY+1;
    if(board->table[y*8+x].pieceColor!=piece->pieceColor){
      num++;
      moves[num].posX=x;
      moves[num].posY=y;
    }
  }
  return num;
}

int checkPawnMoves(chessTable * board, chessPiece * piece, chessMove * moves){
  int num=0, x, y;

  int direction=0;
  if(piece->pieceColor==WHITE_PIECE_COLOR){
    direction--;
  }else{
    direction++;
  }
  //Move positions
  if(board->table[(moves[0].posY+(1*direction))*8+moves[0].posX].pieceColor==0){
    num++;
    moves[num].posX=moves[0].posX;
    moves[num].posY=moves[0].posY+(1*direction);
    if(board->table[(moves[0].posY+(2*direction))*8+moves[0].posX].pieceColor==0 && piece->specialFlag==0){
      num++;
      moves[num].posX=moves[0].posX;
      moves[num].posY=moves[0].posY+(2*direction);
    }
  }
  //Capture positions
  if(moves[0].posX>0){
    x=moves[0].posX-1;
    y=moves[0].posY+(1*direction);

    if((board->table[y*8+x].pieceColor==(piece->pieceColor^3)) || (board->possibleEnPassant && x==board->enPassantMove.posX && y==board->enPassantMove.posY)){
      num++;
      moves[num].posX=moves[0].posX-1;
      moves[num].posY=moves[0].posY+(1*direction);
    }
  }
  if(moves[0].posX<7){
    x=moves[0].posX+1;
    y=moves[0].posY+(1*direction);

    if((board->table[y*8+x].pieceColor==(piece->pieceColor^3)) || (board->possibleEnPassant && x==board->enPassantMove.posX && y==board->enPassantMove.posY)){
      num++;
      moves[num].posX=moves[0].posX+1;
      moves[num].posY=moves[0].posY+(1*direction);
    }
  }
  return num;
}

/*
Recieves the coordinates of the desired position
*/
int getPossibleMoves(chessTable * board, chessMove * moves, int playerToCheck){
  chessPiece * piece=&(board->table[moves[0].posY*8+moves[0].posX]);
  if(piece->pieceColor==playerToCheck){
    switch(piece->pieceType){
      case KING_TYPE :
        return checkKingMoves(board, piece, moves);
      break;
      case QUEEN_TYPE :
        return checkQueenMoves(board, piece, moves);
      break;
      case ROOK_TYPE :
        return checkRookMoves(board, piece, moves);
      break;
      case BISHOP_TYPE :
        return checkBishopMoves(board, piece, moves);
      break;
      case KNIGHT_TYPE :
        return checkKnightMoves(board, piece, moves);
      break;
      case PAWN_TYPE :
        return checkPawnMoves(board, piece, moves);
      break;
    }
  }
  return 0;
}

int isInCheck(chessTable * board){
  int i,j,otherPlayer=getInactivePlayer(board);
  chessMove possibleMoves[MAX_NUMBER_POSSIBLE_MOVES*2];
  chessMove kingPosition;

  for ( i = 0; i < 64; i++) {
      if((board->table+i)->pieceType==KING_TYPE){
        if((board->table+i)->pieceColor==board->activePlayer){
          kingPosition.posX=i%ROW_LENGHT;
          kingPosition.posY=i/ROW_LENGHT;
        }
      }
  }
  for ( i = 0; i < 64; i++) {
    if((board->table+i)->pieceColor==otherPlayer){
      possibleMoves[0].posX=i%ROW_LENGHT;
      possibleMoves[0].posY=i/ROW_LENGHT;
      int numMoves=getPossibleMoves(board, possibleMoves, otherPlayer);
      for (j = 1; j <= numMoves; j++) {
        if(movesAreEqual(kingPosition, possibleMoves[j])){
          return 1;
        }
      }
    }
  }
  return 0;
}

int isMovePossible(int numMoves, chessMove * moves, chessMove moveTo){
  int i;
  for ( i = 1; i <= numMoves; i++) {
    if(moves[i].posX==moveTo.posX && moves[i].posY==moveTo.posY){
      return 1;
    }
  }
  return 0;
}

chessTable * copyChessTable(chessTable * board){
  int i;
  chessTable * newTable=calloc(1,sizeof(chessTable));
  newTable->table=calloc(64,sizeof(chessPiece));
  for ( i = 0; i < 64; i++) {//Makes a copy of the table
    (newTable->table+i)->pieceColor=(board->table+i)->pieceColor;
    (newTable->table+i)->pieceType=(board->table+i)->pieceType;
    (newTable->table+i)->specialFlag=(board->table+i)->specialFlag;
  }
  newTable->activePlayer=board->activePlayer;
  newTable->gameState=board->gameState;
  newTable->possibleEnPassant=board->possibleEnPassant;
  newTable->enPassantMove.posX=board->enPassantMove.posX;
  newTable->enPassantMove.posY=board->enPassantMove.posY;
}

//usado pelos check moves
//ve se o move e valido (nao poe o jogador em check)
int isMoveValid(chessTable * board, chessMove from, chessMove to){
  chessTable * tempBoard = copyChessTable(board);
  movePiece(tempBoard, from, to);
  int inCheck=isInCheck(tempBoard);
  endGame(tempBoard);
  return !inCheck;
}

int getPossibleValidMoves(chessTable * board, chessMove * moves){
  int numMoves=getPossibleMoves(board, moves, board->activePlayer);
  if(numMoves!=0){
    int i=1,j;
    while(i<=numMoves){
      if(!isMoveValid(board, moves[0], moves[i])){
        for (j = i; j < numMoves; j++) {
          moves[j].posX=moves[j+1].posX;
          moves[j].posY=moves[j+1].posY;
        }
        numMoves--;
      }else{
        i++;
      }
    }
  }
  return numMoves;
}

int isThereAnyValidMoves(chessTable * board){
  chessMove possibleMoves[MAX_NUMBER_POSSIBLE_MOVES*2];
  int i;
  for ( i = 0; i < 64; i++) {
    if(board->table[i].pieceColor==board->activePlayer){
      possibleMoves[0].posX=i%ROW_LENGHT;
      possibleMoves[0].posY=i/ROW_LENGHT;
      if(getPossibleValidMoves(board,  possibleMoves)){
          return 1;
      }
    }
  }
  return 0;
}

void pawnPromotion(chessTable * board, chessMove pawnPosition){
  int promoted=0;
  while(!promoted){
    printf("\nWrite piece to promote to(Q-Queen, R-Rook, B-Bishop, K/N-Knight)" );
    switch(getch()){
      case 'Q':
      case 'q':
        board->table[pawnPosition.posY*8+pawnPosition.posX].pieceType=QUEEN_TYPE;
        promoted=1;
        break;
      case 'R':
      case 'r':
        board->table[pawnPosition.posY*8+pawnPosition.posX].pieceType=ROOK_TYPE;
        promoted=1;
        break;
      case 'B':
      case 'b':
        board->table[pawnPosition.posY*8+pawnPosition.posX].pieceType=BISHOP_TYPE;
        promoted=1;
        break;
      case 'N':
      case 'n':
      case 'K':
      case 'k':
        board->table[pawnPosition.posY*8+pawnPosition.posX].pieceType=KNIGHT_TYPE;
        promoted=1;
        break;
    }
  }
}

void play(chessTable * board, int numMoves, chessMove * moves, chessMove moveTo){
  if(isMovePossible(numMoves, moves, moveTo)){
    if(board->gameState==IN_CHECK_STATE){
      board->gameState=NORMAL_STATE;
    }
    movePiece(board, moves[0], moveTo);
    //TODO Improve pawn promotion code
    if(moveTo.posY==0 && board->table[moveTo.posX].pieceType==PAWN_TYPE && board->activePlayer==WHITE_PIECE_COLOR){
        pawnPromotion(board, moveTo);
    }else if(moveTo.posY==7 && board->table[7*8+moveTo.posX].pieceType==PAWN_TYPE && board->activePlayer==BLACK_PIECE_COLOR){
        pawnPromotion(board, moveTo);
    }
    switchPlayer(board);
    int inCheck=isInCheck(board);
    if(isThereAnyValidMoves(board)){
      if(inCheck){
        board->gameState=IN_CHECK_STATE;
      }else{
        board->gameState=NORMAL_STATE;
      }
    }else{
      if(inCheck){
        board->gameState=IN_CHECKMATE_STATE;
      }else{
        board->gameState=IN_DRAW_STATE;
      }
    }
  }
}
