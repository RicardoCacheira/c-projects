#define INTERRUPTED_STATE 0
#define PLAYER_1_WON_STATE 1
#define PLAYER_2_WON_STATE 2
#define DRAW_STATE 3

void inicializeTable(chessTable * board);

void showTable();

void showPossibleMoves(int numMoves, chessMove * moves);

void showChosenColumn(int column);

void showChosenColumnWithPossibleMoves(int numMoves, chessMove * moves, int column);

void printChessBoard();

void finalizeTable();

int runGame();
