#include <stdlib.h>
#include "SRWindow.h"

/*
Color codes to be used in the funcions.
(Note: The colors are considered as the default Xterm colors)
*/
#define TERMINAL_DEFAULT_CODE 0
#define BLACK_CODE 1
#define DARK_RED_CODE 2
#define DARK_GREEN_CODE 3
#define DARK_YELLOW_CODE 4
#define BLUE_CODE 5
#define DARK_MAGENTA_CODE 6
#define DARK_CYAN_CODE 7
#define LIGHT_GRAY_CODE 8
#define DARK_GRAY_CODE 9
#define RED_CODE 10
#define GREEN_CODE 11
#define YELLOW_CODE 12
#define LIGHT_BLUE_CODE 13
#define MAGENTA_CODE 14
#define CYAN_CODE 15
#define WHITE_CODE 16

#define X_BASED_DRAW 0
#define Y_BASED_DRAW 1
#define XY_BASED_DRAW 2

/*
Inicializes the Simple Renderer Window with the given Width and Height and
returns the window pointer.
*/
SRWindow * inicializeWindow(int winWidth,int winHeight);

/*
Clears the recived window.
*/
void clearWindow(SRWindow * window);

/*
Sets the Backgound Color of the recived window.
*/
void setBackgroundColor(SRWindow * window, int backgroundColorCode);

/*
Exports the recived window into a file with the given name.
*/
void exportWindow(SRWindow * window, char * filename);

/*
Imports the window from the file with the given name.
*/
SRWindow * importWindow(char * filename);

/*
Clears the recived window and frees all the reserved memory used by it.
*/
void finalizeWindow(SRWindow * window);

/*
Draws a pixel in the window at the given coordinates and with the given color.
*/
void drawPixel(SRWindow * window, int pixelColorCode, int posX, int posY);

/*
Draws a vertical line in the window starting at the given coordinates with the
given lenght and color.
*/
void drawVerticalLine(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght);

/*
Draws a horizontal line in the window starting at the given coordinates with the
given lenght and color.
*/
void drawHorizontalLine(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght);

/*
Draws a horizontal line in the window starting at the position a and ending at
the position b with the given lenght and color.

The draw mode is the way the line is draw in the window.
X_BASED_DRAW - draws a pixel for every X position
Y_BASED_DRAW - draws a pixel for every Y position
XY_BASED_DRAW - draws a pixel for every X and Y position
*/
void drawLine(SRWindow * window, int pixelColorCode, int posXa, int posYa, int posXb, int posYb, int drawMode);

/*
Draws a rectangle in the window where the top left corner is at the given
coordinates and with the given width, height and color.
*/
void drawRectangle(SRWindow * window, int pixelColorCode, int posX, int posY, int width, int height);

/*
Draws a rectangle in the window with is interior painted where the top left corner
is at the given coordinates and with the given width, height and color.
*/
void drawFilledRectangle(SRWindow * window, int pixelColorCode, int posX, int posY, int width, int height);

/*
Draws a square in the window where the top left corner is at the given
coordinates and with the given lenght and color.
*/
void drawSquare(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght);

/*
Draws a square in the window with is interior painted where the top left corner
is at the given coordinates and with the given lenght and color.
*/
void drawFilledSquare(SRWindow * window, int pixelColorCode, int posX, int posY, int lenght);

/*
Draws the given character in the window at the given coordinates.
If alignToColorLayer is true then the posX is the pixel in that position
If alignToColorLayer is false then the posX is the character in that position
(Note: A pixel is 2 characters in the terminal.)
*/
void drawChar(SRWindow * window, char charToDraw, int posX, int posY,int alignToColorLayer);

/*
Draws the given string in the window starting at the given coordinates.
If alignToColorLayer is true then the posX is the pixel in that position
If alignToColorLayer is false then the posX is the character in that position
(Note: A pixel is 2 characters in the terminal.)
(Note 2: Characters that suppass the window size WILL NOT be draw in the next line)
*/
void drawString(SRWindow * window, char* str, int posX, int posY,int alignToColorLayer);

/*
Renders the given window in the terminal
*/
void renderWindow(SRWindow * window);
