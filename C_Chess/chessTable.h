#include "chessPiece.h"

#define NORMAL_STATE 0
#define IN_CHECK_STATE 1
#define IN_CHECKMATE_STATE 2
#define IN_DRAW_STATE 3
#define IN_PAWN_PROMOTION_STATE 4

typedef struct{
  chessPiece * table;
  int activePlayer;
  int gameState;
  int possibleEnPassant;
  chessMove enPassantMove;
}chessTable;
