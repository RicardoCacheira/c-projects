#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "simpleRenderer.h"
#include "chessLogic.h"
#include "getch.h"

#define SQUARE_PIXEL_SIZE 5

#define INTERRUPTED_STATE 0
#define PLAYER_1_WON_STATE 1
#define PLAYER_2_WON_STATE 2
#define DRAW_STATE 3

#define WHITE_PIECE_COLOR_CODE WHITE_CODE
#define BLACK_PIECE_COLOR_CODE BLACK_CODE

SRWindow * window;
chessTable * boardToDraw;
int whiteSquare;
int blackSquare;
int whiteMoveSquare;
int blackMoveSquare;
int columnChoice;
int chosenPieceColor;
int choseLine;
chessMove moves[MAX_NUMBER_POSSIBLE_MOVES*2];
chessMove moveTo;

void drawPiece(SRWindow * window, chessPiece piece, int posX, int posY){
  int colorX=2+posX*5;
  int charX=colorX*2;
  int pieceY=2+posY*5;
  int i,j;
  int pixelColorCode;
  char pieceText[50];

  switch(piece.pieceType){
    case KING_TYPE :
      strcpy(pieceText,"  _|  |_   |_    _|   _|  |_   (______)  /______\\ ");
      break;
    case QUEEN_TYPE :
      strcpy(pieceText,"()  ()  () \\VVVVVV/   \\\\||//    |____|   |______| ");
      break;
    case ROOK_TYPE :
      strcpy(pieceText," /\\/\\/\\/\\  \\______/   |____|    |____|   |______| ");
      break;
    case BISHOP_TYPE :
      strcpy(pieceText,"   (__)     (    )    _|  |_   (______)  /______\\ ");
      break;
    case KNIGHT_TYPE :
      strcpy(pieceText,"  ____/\\   /      >  \\__    >    /____|   |______|");
      break;
    case PAWN_TYPE :
      strcpy(pieceText,"    __       (__)      /  \\     /____\\   |______| ");
      break;
  }

  if(piece.pieceColor==WHITE_PIECE_COLOR){
    pixelColorCode=WHITE_PIECE_COLOR_CODE;
  }else{
    pixelColorCode=BLACK_PIECE_COLOR_CODE;
  }

  for ( i = 1; i < 4; i++) {
      drawPixel(window,pixelColorCode,colorX+i,pieceY+4);
  }
  for ( i = 0; i < 5; i++) {
    for ( j = 0; j < 10; j++) {
        drawChar(window,pieceText[i*10+j],charX+j,pieceY+i,0);
    }
  }
}

void inicializeTable(chessTable * board){
  whiteSquare=LIGHT_GRAY_CODE;
  blackSquare=DARK_GRAY_CODE;
  whiteMoveSquare=GREEN_CODE;
  blackMoveSquare=DARK_GREEN_CODE;
  columnChoice=GREEN_CODE;
  chosenPieceColor=CYAN_CODE;
  boardToDraw=board;
  window = inicializeWindow(44,44);
}

void drawTable(){
  //setBackgroundColor(window,CYAN_CODE);
  clearWindow(window);
  if(boardToDraw->activePlayer==WHITE_PIECE_COLOR){
    drawString(window, "White Player", 1, 0, 1);
  }else{
    drawString(window, "Black Player", 1, 0, 1);
  }
  if(boardToDraw->gameState==NORMAL_STATE){
    drawSquare(window,BLACK_CODE,0,0,44);
  }else if(boardToDraw->gameState==IN_CHECK_STATE || boardToDraw->gameState==IN_CHECKMATE_STATE){
    drawSquare(window,RED_CODE,0,0,44);
  }else if(boardToDraw->gameState==IN_DRAW_STATE){
    drawSquare(window,BLUE_CODE,0,0,44);
  }

  drawSquare(window,DARK_YELLOW_CODE,1,1,42);

  int i,j;
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      drawFilledSquare(window,whiteSquare,2+(i*10),2+(j*10),5);
      drawFilledSquare(window,blackSquare,7+(i*10),2+(j*10),5);
      drawFilledSquare(window,blackSquare,2+(i*10),7+(j*10),5);
      drawFilledSquare(window,whiteSquare,7+(i*10),7+(j*10),5);
    }
  }
  for ( i = 0; i < 8; i++) {
    drawChar(window,'a'+i,4+(i*5),1,1);
    drawChar(window,'a'+i,4+(i*5),42,1);
    drawChar(window,'8'-i,1,4+(i*5),1);
    drawChar(window,'8'-i,42,4+(i*5),1);
  }
  for (i = 0; i <  8; i++) {
    for (j = 0; j <  8; j++) {
      if(boardToDraw->table[i*8+j].pieceColor){
        drawPiece(window,boardToDraw->table[i*8+j],j,i);
      }
    }
  }
  if(boardToDraw->gameState==IN_CHECKMATE_STATE){
    drawLine(window, RED_CODE, 1, 0, 43, 42, X_BASED_DRAW);
    drawLine(window, RED_CODE, 0, 1, 42, 43, X_BASED_DRAW);
    drawLine(window, RED_CODE, 0, 0, 43, 43, X_BASED_DRAW);
    drawLine(window, RED_CODE, 0, 43, 43, 0, X_BASED_DRAW);
    drawLine(window, RED_CODE, 0, 42, 42, 0, X_BASED_DRAW);
    drawLine(window, RED_CODE, 1, 43, 43, 1, X_BASED_DRAW);
  }
}

void drawBoardSquare(int squareColor, int posX, int posY){
  drawFilledSquare(window,squareColor,2+(posX*5),2+(posY*5),5);
}

void drawChosenColumn(int column){
  drawHorizontalLine(window, columnChoice, 2+column*5, 2+5*8, 5);
  drawHorizontalLine(window, columnChoice, 2+column*5, 1, 5);
}

void drawPossibleMoves(int numMoves, chessMove * moves){
  int i;
  drawBoardSquare(chosenPieceColor, moves[0].posX, moves[0].posY);
  for ( i = 1; i <= numMoves; i++) {
    if((moves[i].posX+moves[i].posY)%2==0){
      drawBoardSquare(whiteMoveSquare, moves[i].posX, moves[i].posY);
    }else{
      drawBoardSquare(blackMoveSquare, moves[i].posX, moves[i].posY);
    }
  }
}

void showTable(){
  drawTable();
  renderWindow(window);
}

void showChosenColumn(int column){
  drawTable();
  drawChosenColumn(column);
  renderWindow(window);
}

void showPossibleMoves(int numMoves, chessMove * moves){
  drawTable();
  drawPossibleMoves(numMoves, moves);
  renderWindow(window);
}

void showChosenColumnWithPossibleMoves(int numMoves, chessMove * moves, int column){
  drawTable();
  drawChosenColumn(column);
  drawPossibleMoves(numMoves, moves);
  renderWindow(window);
}

void printChessBoard(){
  exportWindow(window, "chess.srwindow");
}

void finalizeTable(){
  finalizeWindow(window);
}

void numberChoice(int num){
  if(choseLine==0){
    moves[0].posX=num;
    choseLine=1;
  }else if(choseLine==1){
    moves[0].posY=7-num;
    choseLine=2;
  }else if(choseLine==2){
    moveTo.posX=num;
    choseLine=3;
  }else if(choseLine==3){
    moveTo.posY=7-num;
    choseLine=4;
  }
}

void charChoice(int charPosition){
  if(choseLine==0){
    moves[0].posX=charPosition;
    choseLine=1;
  }else if(choseLine==2){
    moveTo.posX=charPosition;
    choseLine=3;
  }
}

int runGame(){
  int endState;
  chessTable * board=startGame();
  //chessTable * board=startCustomGame("2R2N2B2Q2K2B2N2R2P2P2P2P2P2P2P2P000000000000000000000000000000001P1P1P1P1P1P1P1P1R1N1B1Q1K1B1N1R");
  inicializeTable(board);
  int endOfGame=0, numMoves;
  choseLine=0;
  showTable();
  while(!endOfGame){
    switch(getch()){
      case 'a':
      case 'A':
        charChoice(0);
        break;
      case 'b':
      case 'B':
        charChoice(1);
        break;
      case 'c':
      case 'C':
        charChoice(2);
        break;
      case 'd':
      case 'D':
        charChoice(3);
        break;
      case 'e':
      case 'E':
        charChoice(4);
        break;
      case 'f':
      case 'F':
        charChoice(5);
        break;
      case 'g':
      case 'G':
        charChoice(6);
        break;
      case 'h':
      case 'H':
        charChoice(7);
        break;
      case '1':
        numberChoice(0);
        break;
      case '2':
        numberChoice(1);
        break;
      case '3':
        numberChoice(2);
        break;
      case '4':
        numberChoice(3);
        break;
      case '5':
        numberChoice(4);
        break;
      case '6':
        numberChoice(5);
        break;
      case '7':
        numberChoice(6);
        break;
      case '8':
        numberChoice(7);
        break;
      case 'q':
      case 'Q':
        endOfGame=1;
        endState=INTERRUPTED_STATE;
        break;
      case 'p':
      case 'P':
        printChessBoard();
        break;
      default:
        showTable();
        choseLine=0;
    }
    if(choseLine==1){
      showChosenColumn(moves[0].posX);
    }else if(choseLine==2){
      numMoves=getPossibleValidMoves(board, moves);
      if(numMoves!=0){
        showPossibleMoves(numMoves, moves);
      }else{
        choseLine=0;
        showTable();
      }
    }else if(choseLine==3){
      showChosenColumnWithPossibleMoves(numMoves, moves, moveTo.posX);
    }else if(choseLine==4){
      play(board,numMoves, moves, moveTo);
      //Checks if the game ended
      if(board->gameState==IN_CHECKMATE_STATE){
        endOfGame=1;
        //The active player was changed in the play function so the active player is the one who's lost.
        if(board->activePlayer==1){
          endState=PLAYER_2_WON_STATE;
        }else{
          endState=PLAYER_1_WON_STATE;
        }
      }else if(board->gameState==IN_DRAW_STATE){
        endOfGame=1;
        endState=DRAW_STATE;
      }else{
        showTable();
        choseLine=0;
      }
    }
  }
  endGame(board);
  finalizeTable();
  return board->gameState;
}
