#define WHITE_PIECE_COLOR 1
#define BLACK_PIECE_COLOR 2
#define KING_TYPE 'K'
#define QUEEN_TYPE 'Q'
#define ROOK_TYPE 'R'
#define BISHOP_TYPE 'B'
#define KNIGHT_TYPE 'N'
#define PAWN_TYPE 'P'

typedef struct{
  char pieceColor;
  char pieceType;
  char specialFlag;
}chessPiece;

typedef struct{
  char posX;
  char posY;
}chessMove;
